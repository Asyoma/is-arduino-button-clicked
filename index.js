const five = require('johnny-five');
const board = new five.Board();
const express = require('express')
const cors = require('cors')
const exec = require('child_process').exec;
const path = require('path');
const fs = require('fs');
const app = express()
const port = 3001;
const LineReader = require('node-line-reader').LineReader;
var buttonClicked_arr=[];
var buttonClicked_file=path.join(__dirname, "buttonClicked.txt");
var buttonUnclicked_arr=[];
var buttonUnclicked_file=path.join(__dirname, "buttonUnclicked.txt");
var button;

var is_btn_clicked=false;

try {
    if (fs.existsSync(buttonUnclicked_file)) {
        readFile_buttonUnclicked();
    }
    else{
        fs.writeFile(buttonUnclicked_file,"",function(){});
        buttonUnclicked_arr=["echo 'Unclicked'", "echo 'Unclicked 2'"]
    }
} catch(err) {
    console.error(err)
}

try {
    if (fs.existsSync(buttonClicked_file)) {
        readFile_buttonClicked();
    }
    else{
        fs.writeFile(buttonClicked_file,"",function(){});
        buttonClicked_arr = ["echo 'button clicked'","echo 'button clicked 2'"]
    }
} catch(err) {
    console.error(err)
}

board.on("ready", function() {
    
    
    // Create a new `button` hardware instance.
    // This example allows the button module to
    // create a completely default instance
    button = new five.Button(2);
  
    // Inject the `button` hardware into
    // the Repl instance's context;
    // allows direct command line access
    board.repl.inject({
      button: button
    });
  
    // Button Event API
  
    // "down" the button is pressed
    button.on("down", function() {
      btn_click();
    });
  
    // "up" the button is released
    button.on("up", function() {
      btn_unclick()
    });
  });
  


  app.use(cors())

app.get('/', (req, res) => {
    res.send('Arduino is clicked app')
});

app.get('/info', (req, res) => {
    
    res.send('Arduino is clicked app')
});

app.get('/click', (req, res) => {
    btn_click();
    res.send('Button clicked')
});

app.get('/unclick', (req, res) => {
    btn_unclick();
    res.send('Button unclicked')
});


 




app.listen(port, () => console.log(`Terminal arduino button is clicked checker started on port = ${port}!`))

function btn_click(){
    if(is_btn_clicked == false){
        var stat = true;

        for (var i = 0, len = buttonClicked_arr.length; i < len; i++) {
            if(stat == true){
                executeApp(buttonClicked_arr[i],function(err){
                    if(err){
                        stat =false;
                        console.error(err);
                    }
                    else{
                        is_btn_clicked = true;
                    }
                })
            }
        }
    }
}

function btn_unclick(){
    if(is_btn_clicked ==true){
        var stat = true;

        for (var i = 0, len = buttonUnclicked_arr.length; i < len; i++) {
            if(stat == true){
                executeApp(buttonUnclicked_arr[i],function(err){
                    if(err){
                        stat =false;
                        console.error(err);
                    }
                    else{
                        is_btn_clicked = false;
                    }
                })
            }
        }
    }
}

function executeApp(appString,cb){
    exec(appString, function (err, stdout, stderr) {
        console.log(stdout);
        console.error(stderr);
        cb(err);
    });
}


function readFile_buttonClicked(){
    var reader = new LineReader(buttonClicked_file);
    reader.nextLine(function (err, line) {
        if (!err) {
            buttonClicked_arr.push(line);
            console.log('buttonClicked line: ', line);
        }
    });
    
}

function readFile_buttonUnclicked(){
    var reader = new LineReader(buttonUnclicked_file);
    reader.nextLine(function (err, line) {
        if (!err) {
            buttonUnclicked_arr.push(line);
            console.log('buttonUnclicked line: ', line);
        }
    });
    
}